<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Firebase\JWT\JWT as JWTlib;
use Firebase\JWT\Key;

class Jwt {

    private $CI;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->config('jwt');
    }

    public function encode($data) {
        $key = $this->CI->config->item('jwt_key');
        $algorithm = $this->CI->config->item('jwt_algorithm');
        $issuer = $this->CI->config->item('jwt_issuer');
        $audience = $this->CI->config->item('jwt_audience');
        $expire = $this->CI->config->item('jwt_expire');
    
        $token = [
            'iss' => $issuer,
            'aud' => $audience,
            'iat' => time(),
            'exp' => time() + $expire,
            'data' => $data
        ];

        return JWTlib::encode($token, $key, $algorithm);
    }

}
