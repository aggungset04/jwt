<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['jwt_key'] = '3fc3492dbefe70317e210170fbf920cedec1d295a36c187b74b8f0ccf1b759e4';
$config['jwt_algorithm'] = 'HS256';
$config['jwt_issuer'] = 'https://serverprovider.com';
$config['jwt_audience'] = 'https://serverclient.com';
$config['jwt_expire'] = 3600;
