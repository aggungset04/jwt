<?php

class Kontak_model extends CI_Model {

    public function insert_sample_data()
    {
        $data = array(
            array(
                'name' => 'Nama 1',
                'number' => '1234567890'
            ),
            array(
                'name' => 'Nama 2',
                'number' => '0987654321'
            )
            // Tambahkan lebih banyak array data di sini jika perlu.
        );

        $this->db->insert_batch('kontak', $data);
    }
}
